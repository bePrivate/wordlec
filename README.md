# Wordle

Yet another recreation of the game Wordle (now in C)  

The wordlist is taken from https://github.com/tabatkins/wordle-list.

This program is distributed WITHOUT WARRANTY of any kind.
Proven working on Linux ONLY. Contributions welcome!

# Usage

1. Clone the repo  
`git clone https://gitlab.com/bePrivate/wordlec`

2. Change into the cloned directory  
`cd wordlec`

3. Install the game  
`make && make install`  

4. Run the game  
`wordlec` (`wordlec -h/--help` for help)  

# How to play

After starting, you are prompted to enter a word. When you enter it, you will see it on
the screen. You will see whether each letter is at the correct place or not, or if it
even is in the word in the first place. You have six tries, then you lose and the correct
word is shown to you. If you win, the number of guesses is displayed, along with the
correct word.
