/*
	Wordle game implementation in the C programming language
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <getopt.h>

static char RESET[20];     
static char INCORRECT[20];       
static char CORRECT[20];         
static char OUTOFPLACE[20];      
static char ASCII[20];

static char INCORRECT_USER[20];
static char CORRECT_USER[20];
static char OUTOFPLACE_USER[20];
static char ASCII_USER[20];

int flag_light_theme   = 0;
int flag_user_theme    = 0;
int flag_verbose       = 0;
int flag_wordlist      = 0;
int len_wordlist       = 0;
int lose               = 0;
char pf_props[6][5][2] = {
	// <char>	<colorcode>  	 
	// letter 	0, 1, 2, 3 or 4 where 
	//				0 (RESET) 		is not yet guessed 
	// 				1 (INCORRECT) 	default red, is incorrect,
	// 				2 (CORRECT) 	default green, is correct and  
	//				3 (OUTOFPLACE)	default yellow, is out-of-place

	{ " 0", " 0", " 0", " 0", " 0" },
	{ " 0", " 0", " 0", " 0", " 0" },
	{ " 0", " 0", " 0", " 0", " 0" },
	{ " 0", " 0", " 0", " 0", " 0" },
	{ " 0", " 0", " 0", " 0", " 0" },
	{ " 0", " 0", " 0", " 0", " 0" }
};
char playfield[6][5] = {
	{ ' ', ' ', ' ', ' ', ' ' },
	{ ' ', ' ', ' ', ' ', ' ' },
	{ ' ', ' ', ' ', ' ', ' ' },
	{ ' ', ' ', ' ', ' ', ' ' },
	{ ' ', ' ', ' ', ' ', ' ' },
	{ ' ', ' ', ' ', ' ', ' ' }
};
int quit             = 0;
int turns            = 0;
int win              = 0;
char wordlist_path[1024];
char wordlist[16384][6]; // 2^14
char word_to_guess[5];

/*
 *
 *
 * END OF GLOBAL VARIABLES
 * =======================
 * START OF FUNCTIONS
 *
 *
 */ 

int argparse(int argc, char *argv[]);
void clear();
void eval_lose();
void eval_props(char word[]);
void eval_win(int i);
void eval_word(char word[]);
int file_exists(char path[]);
void game();
void get_random_word(char word_to_guess[], char wordlist_path[]);
int main(int argc, char *argv[]);
void print_help_and_usage();
void prompt(char user_input[]);
void put_word_into_playfield(char word[]);
int search_word(char word[], char target);
int search_wordlist(char wordlist[][6], char target[]);
void set_theme();
void set_user_theme();
void show();
void show_win_lose_message();

/*
	pass the arguments
*/
int argparse(int argc, char *argv[]) {
	int flag_error = 0;

	int option;
	char theme_values[4][2] = { "  ", "  ", "  ", "  " };
    int option_index = 0;

    static struct option long_options[] = {
        {"theme",       required_argument, 0, 't'},
        {"wordlist",    required_argument, 0, 'w'},
        {"light-theme", no_argument,       0, 'l'},
        {"verbose",     no_argument,       0, 'v'},
        {"help",        no_argument,       0, 'h'},
        {0, 0, 0, 0}
    };

    while ((option = getopt_long(argc, argv, "hvt:w:l", long_options, &option_index)) != -1) {
        switch (option) {
        	case 'h':
          		print_help_and_usage();
				return 2;
			case 'v':
				flag_verbose = 1;
				break;
            case 't':
            	if (strlen(optarg) != 6) {
            		fprintf(stderr, "Invalid value. -h/--help for help.\n");
            		flag_error = 1;
            	} else {
		            for (int i = 0, c = 0; i < 3, c < 6; i++, c += 2) {
		            	sprintf(theme_values[i], "%c%c", optarg[c], optarg[c+1]);
		            }
		            flag_user_theme = 1;
		            set_user_theme(theme_values[0], theme_values[1], theme_values[2]);
		            if (flag_verbose == 1) printf("user-specified theme on\n");
		        }
		        break;
            case 'w':
                strcpy(wordlist_path, optarg);
                flag_wordlist = 1;
                if (flag_verbose == 1) printf("using wordlist %s\n", wordlist_path);
                break;
            case 'l':
            	flag_light_theme = 1;
            	if (flag_verbose == 1) printf("light theme on\n");
            	break;
            default:
            	fprintf(stderr, "Invalid argument\n");
            	flag_error = 1;
            	break;
        }
        if (flag_error == 1) break;
    }

    if (flag_wordlist == 0) {
    	strcpy(wordlist_path, "lib/wordlist");
    	if (flag_verbose == 1) printf("wordlist_path gets the default path /usr/lib/wordlists/wordlist\n");
    }

    return flag_error;
}

/*
	clears the console/screen
*/
void clear() {
	printf("\e[1;1H\e[2J");
}

/*
	evaluate the loss
*/
void eval_lose() {
	if (turns >= 5 && win == 0) {
		lose = 1;
	}
}

/*
	helper function for evaluating the properties of a word
*/
void eval_props(char word[]) {
	for (int i = 0; i < 5; i++) {
		if (word_to_guess[i] == word[i]) {
			pf_props[turns][i][0] = word[i];
			pf_props[turns][i][1] = '2';
		} else if (search_word(word_to_guess, word[i]) != -1 && word_to_guess[i] != word[i]) {
			pf_props[turns][i][0] = word[i];
			pf_props[turns][i][1] = '3';
		} else if (search_word(word_to_guess, word[i]) == -1) {
			pf_props[turns][i][0] = word[i];
			pf_props[turns][i][1] = '1';			
		}
	}
}

/*
	evaluate the win
*/
void eval_win(int i) {
	if (strcmp(wordlist[i], word_to_guess) == 0) {
		win = 1;
	}
}

/*
	evaluate the word
*/
void eval_word(char word[]) {
	if (search_wordlist(wordlist, word) != -1) {
		eval_win(search_wordlist(wordlist, word));
		eval_lose();
		eval_props(word); // eval correct/incorrect/oop letters
		put_word_into_playfield(word);
	} else {
		clear();
		printf("Word %s not in wordlist\n", word);
		sleep(2);
	}
}

/*
	tests if a file exists
	return 0 - yes
	return 1 - no
*/
int file_exists(char path[]) {
	if (access(path, F_OK) != 0) {
		return 0;
	} else {
		return 1;
	}
}

/*
	# prerequisites:    
	& 1 choose a random word from a wordlist 

	# algo steps:
	& 1 clear the screen
	& 2 display the current state of the playfield
	& 3 prompt the user to enter a word
	& 4 evaluate the word
	& 5 print the word on the screen with the corresponding parameters at each letter (right position, wrong position, not in word)
	& 6.1 if the user entered the right word, show a winning message and exit
	& 6.2 if the user has tried 6 times already, show a losing message, the word and exit
*/
void game() {
	// step 1 & 2
	show();

	// step 3
	char user_input[5];
	prompt(user_input);

	// implemented a quit function
	if (user_input[0] == 'q') quit = 1;

	// step 4 & 5
	eval_word(user_input);
}

/*
	gets a random word from the given wordlist
*/
void get_random_word(char word_to_guess[], char wordlist_path[]) {
	FILE* wordlist_file = fopen(wordlist_path, "r");
	if (wordlist_file == NULL) {
        perror("Error opening file");
    }
	char temp_word[6];
	while (fscanf(wordlist_file, "%5s", temp_word) == 1 ) {
		strcpy(wordlist[len_wordlist], temp_word);
		len_wordlist++;
	}

	srand(time(NULL));

	strcpy(word_to_guess, wordlist[rand() % len_wordlist]);

	fclose(wordlist_file);
}

/*
	driver code
*/
int main(int argc, char *argv[]) {
	// pass the arguments
	int args = argparse(argc, argv);
	switch (args) {
	case 1:
		return 1; // error occured
	case 2:
		return 0; // --help passed
	}

	// set light/dark theme
	if (flag_user_theme == 0) set_theme();

	// prerequisite 1
	get_random_word(word_to_guess, wordlist_path);

	// steps
	while (win == 0 && lose == 0) {
		game();
		if (quit == 1) return 2;
	}

	// print winning message
	show_win_lose_message();

	return 0;
}

/*
	prints the help & usage for the user to know what to do
*/
void print_help_and_usage() {
	fprintf(stdout,
		"Help and usage for wordlec:\n"
		"\n"
		"Help:\n"
		"-w/--wordlist <path_to_wordlist_file>\n"
		"    specify a custom wordlist\n"
		"        RULES: five characters on each line (no empty lines),\n"
		"               each new line is separated by '\\n' (Enter/Return)\n"
		"-t/--theme <correct_color> <incorrect_color> <out_of_place_color>\n"
		"    specify the colors in which the playfield will be visible\n"
		"        RULES: choose the color from this list:\n"
		"               [ 'b' (black), 'r' (red), 'g' (green), 'y' (yellow), 'l' (blue), 'm' (magenta), 'c' (cyan), 'w' (white) ]\n"
		"               the format is <foreground_color><background_color>\n"
		"        EXAMPLE: '-t rwgwlw'\n"
		"-l/--light-theme\n"
		"    activate light theme\n"
		"-v/--verbose\n"
		"    be verbose (show what the program is doing aka debug info)\n"
		"-h/--help\n"
		"    show this help screen\n"
	);
}

/*
	prompt the user for their input
*/
void prompt(char user_input[]) {
	int c;
	printf(">> ");
	scanf("%5s", user_input);
	while ((c = getchar()) != '\n' && c != EOF);
}

/*
	put a valid word into the playfield
*/
void put_word_into_playfield(char word[]) {
	strcpy(playfield[turns], word);
	turns++;

	// resetting the rest of the playfield
	for (int i = turns; i < turns + (6 - turns); i++) {
		strcpy(playfield[i], "     ");
	}
}

/*
	helper function for searching within a string
*/
int search_word(char word[], char target) {
	for (int i = 0; i < strlen(word); i++) {
		if (word[i] == target) return i;
	}
	return -1;	
}

/*
	linear search helper function for searching within the wordlist
*/
int search_wordlist(char wordlist[][6], char target[]) {
	for (int i = 0; i < len_wordlist; i++) {
		if (strcmp(wordlist[i], target) == 0) return i;
	}
	return -1;
}

/*
	set the theme (light/dark/user-specified)
*/
void set_theme() {
	if (flag_user_theme != 1) {
		// handle light/dark mode
		if (flag_light_theme == 1) {
			snprintf(RESET,      sizeof(RESET),      "\x1b[0;0m");
			snprintf(INCORRECT,  sizeof(INCORRECT),  "\x1b[41;41m");
			snprintf(CORRECT,    sizeof(CORRECT),    "\x1b[42;42m");
			snprintf(OUTOFPLACE, sizeof(OUTOFPLACE), "\x1b[42;43m");
			snprintf(ASCII,      sizeof(ASCII),      "\x1b[37;37m");
		} else {
			snprintf(RESET,      sizeof(RESET),      "\x1b[30;0m");
			snprintf(INCORRECT,  sizeof(INCORRECT),  "\x1b[30;41m");
			snprintf(CORRECT,    sizeof(CORRECT),    "\x1b[30;42m");
			snprintf(OUTOFPLACE, sizeof(OUTOFPLACE), "\x1b[30;43m");
			snprintf(ASCII,      sizeof(ASCII),      "\x1b[30;90m");
		}
	} else {
		// handle user-specified theme
		snprintf(RESET,         sizeof(RESET),      "\x1b[0;0m");
		snprintf(INCORRECT,     sizeof(INCORRECT),  "%s", INCORRECT_USER);
		snprintf(CORRECT,       sizeof(CORRECT),    "%s", CORRECT_USER);
		snprintf(OUTOFPLACE,	sizeof(OUTOFPLACE), "%s", OUTOFPLACE_USER);
		//snprintf(ASCII,      	sizeof(ASCII),		"%s", ASCII_USER);	
	}
}

/*
	set user-specified theme
*/
void set_user_theme(char incorrect[], char correct[], char outofplace[]) {
	/* b = black
	 * r = red
	 * g = green
	 * y = yellow
	 * l = blue
	 * m = magenta
	 * c = cyan
	 * w = white
	 */

	int incorrect_int[2];
	int correct_int[2];
	int outofplace_int[2];

	// converting the flags into usable numbers
	for (int i = 0; i < 2; i++) {
		switch (incorrect[i]) {
		case 'b':
			if (i == 0) incorrect_int[i] = 30;
			if (i == 1) incorrect_int[i] = 40;
			break;
		case 'r':
			if (i == 0) incorrect_int[i] = 31;
			if (i == 1) incorrect_int[i] = 41;
			break;
		case 'g':
			if (i == 0) incorrect_int[i] = 32;
			if (i == 1) incorrect_int[i] = 42;
			break;
		case 'y':
			if (i == 0) incorrect_int[i] = 33;
			if (i == 1) incorrect_int[i] = 43;
			break;
		case 'l':
			if (i == 0) incorrect_int[i] = 34;
			if (i == 1) incorrect_int[i] = 44;
			break;
		case 'm':
			if (i == 0) incorrect_int[i] = 35;
			if (i == 1) incorrect_int[i] = 45;
			break;
		case 'c':
			if (i == 0) incorrect_int[i] = 36;
			if (i == 1) incorrect_int[i] = 46;
			break;
		case 'w':
			if (i == 0) incorrect_int[i] = 37;
			if (i == 1) incorrect_int[i] = 47;
			break;
		}
	}

	for (int i = 0; i < 2; i++) {
		switch (correct[i]) {
		case 'b':
			if (i == 0) correct_int[i] = 30;
			if (i == 1) correct_int[i] = 40;
			break;
		case 'r':
			if (i == 0) correct_int[i] = 31;
			if (i == 1) correct_int[i] = 41;
			break;
		case 'g':
			if (i == 0) correct_int[i] = 32;
			if (i == 1) correct_int[i] = 42;
			break;
		case 'y':
			if (i == 0) correct_int[i] = 33;
			if (i == 1) correct_int[i] = 43;
			break;
		case 'l':
			if (i == 0) correct_int[i] = 34;
			if (i == 1) correct_int[i] = 44;
			break;
		case 'm':
			if (i == 0) correct_int[i] = 35;
			if (i == 1) correct_int[i] = 45;
			break;
		case 'c':
			if (i == 0) correct_int[i] = 36;
			if (i == 1) correct_int[i] = 46;
			break;
		case 'w':
			if (i == 0) correct_int[i] = 37;
			if (i == 1) correct_int[i] = 47;
			break;
		}
	}

	for (int i = 0; i < 2; i++) {
		switch (outofplace[i]) {
		case 'b':
			if (i == 0) outofplace_int[i] = 30;
			if (i == 1) outofplace_int[i] = 40;
			break;
		case 'r':
			if (i == 0) outofplace_int[i] = 31;
			if (i == 1) outofplace_int[i] = 41;
			break;
		case 'g':
			if (i == 0) outofplace_int[i] = 32;
			if (i == 1) outofplace_int[i] = 42;
			break;
		case 'y':
			if (i == 0) outofplace_int[i] = 33;
			if (i == 1) outofplace_int[i] = 43;
			break;
		case 'l':
			if (i == 0) outofplace_int[i] = 34;
			if (i == 1) outofplace_int[i] = 44;
			break;
		case 'm':
			if (i == 0) outofplace_int[i] = 35;
			if (i == 1) outofplace_int[i] = 45;
			break;
		case 'c':
			if (i == 0) outofplace_int[i] = 36;
			if (i == 1) outofplace_int[i] = 46;
			break;
		case 'w':
			if (i == 0) outofplace_int[i] = 37;
			if (i == 1) outofplace_int[i] = 47;
			break;
		}
	}

	int incorrect_f  = incorrect_int[0];
	int incorrect_b  = incorrect_int[1];
	int correct_f    = correct_int[0];
	int correct_b    = correct_int[1];
	int outofplace_f = outofplace_int[0];
	int outofplace_b = outofplace_int[1];

	snprintf(INCORRECT_USER,  sizeof(INCORRECT_USER),  "\x1b[%d;%dm", incorrect_f,  incorrect_b);
	snprintf(CORRECT_USER,    sizeof(CORRECT_USER),    "\x1b[%d;%dm", correct_f,    correct_b);
	snprintf(OUTOFPLACE_USER, sizeof(OUTOFPLACE_USER), "\x1b[%d;%dm", outofplace_f, outofplace_b);
	//snprintf(ASCII_USER,      sizeof(ASCII_USER),      "\x1b[%d;%dm", ascii_f,      ascii_b);

	set_theme();
}

/*
	prints the current state of the game
	shows 
		correct letters
		incorrect letters
		and out-of-place letters
		also the previous attempts
*/
void show() {
	clear();
	printf("%s╔═══╦═══╦═══╦═══╦═══╗%s\n", ASCII, RESET);
	for (int i = 0; i < 6; i++) {
		for (int j = 0; j < 5; j++) {
			switch (pf_props[i][j][1]) {
			case '0':
				printf("%s║ %c ", ASCII, playfield[i][j]);
				break;
			case '1':
				printf("%s║%s %c %s", ASCII, INCORRECT, playfield[i][j], RESET);
				break;
			case '2':
				printf("%s║%s %c %s", ASCII, CORRECT, playfield[i][j], RESET);
				break;
			case '3':
				printf("%s║%s %c %s", ASCII, OUTOFPLACE, playfield[i][j], RESET);
				break;
			}
		}
		printf("%s║%s\n", ASCII, RESET);
		if (i < 5) printf("%s╠═══╬═══╬═══╬═══╬═══╣%s\n", ASCII, RESET);
	}
	printf("%s╚═══╩═══╩═══╩═══╩═══╝%s\n", ASCII, RESET);
}

/*
	shows the message after the game ends
*/
void show_win_lose_message() {
	show();
	if (win == 1 && lose == 0) fprintf(stdout, "You won! It took you %d guesses. The word was %s.\n", turns, word_to_guess);
	if (win == 0 && lose == 1) fprintf(stdout, "You lost. :( The word was %s.\n", word_to_guess);
}