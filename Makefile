first:
	gcc src/main.c -o main

install:
	sudo cp ./main /usr/bin/wordlec
	sudo mkdir -p /usr/lib/wordlists
	sudo cp ./lib/wordlist /usr/lib/wordlists/
run:
	./main
